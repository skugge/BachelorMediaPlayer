# Sources
This system relies heavily on the dab-cmdline and websocketpp libraries.

## The following sources have been used:

* JvanKatwijk, dab-cmdline: DAB decoding library with example of its use. [Online]. Available: https://github.com/JvanKatwijk/dab-cmdline. [Accessed: Jan-2018].
* P. Thorson, websocketpp: C++ websocket client/server library. [Online]. Available: https://github.com/zaphoyd/websocketpp. [Accessed: Feb-2018].
* “RFC 6455 - The WebSocket Protocol." [Online]. Available: https://tools.ietf.org/html/rfc6455. [Accessed: Feb-2018].
* “c++ - How to use OpenSSL’s SHA256 functions - Stack Overflow.” [Online]. Available: https://stackoverflow.com/questions/13784434/how-to-use-openssls-sha256-functions/13784507. [Accessed: 13-Apr-2018].
* “c++ - How to obtain (almost) unique system identifier in a cross platform way?,” Stack Overflow, 16-Apr-2018. [Online]. Available: https://stackoverflow.com/questions/16858782/how-to-obtain-almost-unique-system-identifier-in-a-cross-platform-way. [Accessed: 16-Apr-2018].
* “c++ - appending to a file with ofstream,” Stack Overflow. [Online]. Available: https://stackoverflow.com/questions/26084885/appending-to-a-file-with-ofstream. [Accessed: 23-May-2018].
