# BachelorMediaPlayer

## Build

1. Go to backend directory
	
	```console
foo@bar:~$ cd dab-cmdline/backend
	```

2. Create build folder

	```console
foo@bar:~$ mkdir build
	```

3. Build the project

	```console
foo@bar:~$ cmake .. -DRTLSDR=ON
	```

4. Create executable

	```console
foo@bar:~$ make
	```

5. Run 

	```console
foo@bar:~$ ./dab-rtlsdr-2
	```