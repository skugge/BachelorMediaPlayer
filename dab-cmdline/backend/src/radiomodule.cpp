#include <iostream>
#include <Python.h>
#include "radio.cpp"

//static PyObject *SpamError;


int Number(int n){
  if (n ==1){
    std::cout << "Making a Ringbuffer since you sendt 1." << std::endl;
  }else{
    std::cout << "The number you entered was not equal to one." << std::endl;
    return n;
  }
  return 1;
}

static PyObject *
radio_system(PyObject *self, PyObject *args)
{
    int n = 0;

    if (!PyArg_ParseTuple(args, "i", &n))
        return NULL;
    
    return Py_BuildValue("i",Number(n));
}


static PyMethodDef RadioMethods[] = {
    {"system",  radio_system, METH_VARARGS,
     "Execute a shell command."},
    {NULL, NULL, 0, NULL}      
};


static struct PyModuleDef radiomodule = {
    PyModuleDef_HEAD_INIT,
    "radio",   /* name of module */
    NULL,      /* module documentation, may be NULL */
    -1,        /* size of per-interpreter state of the module,
                 or -1 if the module keeps state in global variables. */
    RadioMethods
};

extern "C" { 
PyMODINIT_FUNC
PyInit_radio(void)
  {
    return PyModule_Create(&radiomodule);
  }
}
