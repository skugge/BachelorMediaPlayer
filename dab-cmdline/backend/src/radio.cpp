/*
 *    Copyright (C) 2015, 2016, 2017
 *    Jan van Katwijk (J.vanKatwijk@gmail.com)
 *    Lazy Chair Computing
 *
 *    This file is part of the DAB-library
 *
 *    DAB-library is free software; you can redistribute it and/or modify
 *    it under the terms of the GNU General Public License as published by
 *    the Free Software Foundation; either version 2 of the License, or
 *    (at your option) any later version.
 *
 *    DAB-library is distributed in the hope that it will be useful,
 *    but WITHOUT ANY WARRANTY; without even the implied warranty of
 *    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *    GNU General Public License for more details.
 *
 *    You should have received a copy of the GNU General Public License
 *    along with DAB-library; if not, write to the Free Software
 *    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 *	E X A M P L E  P R O G R A M
 *	for the DAB-library
 */



#include	<unistd.h>
#include	<signal.h>
#include    <thread>    
#include	<getopt.h>
#include    <cstdio>
#include    <iostream>
#include	"audiosink.h"
#include	"filesink.h"
#include	"dab-class.h"
#include	"band-handler.h"
#ifdef	HAVE_SDRPLAY
#include	"sdrplay-handler.h"
#elif	HAVE_AIRSPY
#include	"airspy-handler.h"
#elif	HAVE_RTLSDR
#include	"rtlsdr-handler.h"
#elif	HAVE_WAVFILES
#include	"wavfiles.h"
#elif	HAVE_RAWFILES
#include	"rawfiles.h"
#elif	HAVE_RTL_TCP
#include	"rtl_tcp-client.h"
#endif
#include	<locale>
#include	<codecvt>
#include	<atomic>
#ifdef	DATA_STREAMER
#include	"tcp-server.h"
#endif
//#include "sio_client.h"
//#include <openssl>
#include <boost/asio/ssl.hpp>
#include <boost/asio/ssl/context.hpp>
#include <boost/bind.hpp>
#include <boost/thread.hpp>
#include <boost/thread/mutex.hpp>
#include <websocketpp/config/asio_client.hpp>
#include <websocketpp/client.hpp>
#include <websocketpp/config/asio.hpp>
#include <sys/utsname.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <set>
#include <future>


typedef websocketpp::client<websocketpp::config::asio_tls_client> client;
typedef websocketpp::lib::shared_ptr<boost::asio::ssl::context> context_ptr;
typedef websocketpp::config::asio_client::message_type::ptr message_ptr;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;


using namespace std;
using std::cerr;
using std::endl;


mutex m;
condition_variable cv;

bool ready = false;


static std::string Channel;
static int16_t waitingTime;
static uint8_t band;
static std::atomic<bool> timeSynced;
static std::atomic<bool> timesyncSet;
static std::atomic<bool>ensembleRecognized;
static std::atomic<bool> run;
static dabClass	*theRadio;
static audioBase	*soundOut;
static bandHandler	dabBand;
static deviceHandler *theDevice;
struct sigaction sigact;
static std::vector<std::string> programNames;
static string station_list;
static std::vector<int> programSIds;
static std::string programName = "NRK P13";
static std::string filename = "stations.txt";
static bool found = false;
#ifdef	DATA_STREAMER
tcpServer	tdcServer (8888);
#endif



class radio{

    int32_t		serviceIdentifier	= -1;

    public:
    /// Sigint handler
    /// \param signum
    static void sighandler (int signum) {
        fprintf (stderr, "Signal caught, terminating!\n");
        run.store (false);
    }
    /// Signal handler
    /// \param b
    /// \param userData
    static void syncsignalHandler (bool b, void *userData) {
        timeSynced. store (b);
        timesyncSet. store (true);
        (void)userData;
    }


    ///	This function is called whenever the dab engine has taken
    ///	some time to gather information from the FIC bloks
    ///	the Boolean b tells whether or not an ensemble has been
    ///	recognized, the names of the programs are in the
    ///	ensemble
    /// \param name
    /// \param Id
    /// \param userData
    static void ensemblenameHandler (std::string name, int Id, void *userData) {
        fprintf (stderr, "ensemble %s is (%X) recognized\n",
                 name. c_str (), (uint32_t)Id);
        ensembleRecognized.store (true);
    }


    /// Programname handler
    /// \param s
    /// \param SId
    /// \param userdata
    static void programnameHandler (std::string s, int SId, void *userdata) {
        found = true;
        for (std::vector<std::string>::iterator it = programNames.begin();
             it != programNames. end(); ++it)
            if (*it == s)
                return;
        programNames.push_back (s);
        programSIds .push_back (SId);
        //overview.insert(pair);
        station_list += s + "," + Channel + "\n";
        writeFile("channels.txt",s + "," + Channel + "\n",true);
        std::cerr << "program " << s << " is part of the ensemble\n";
    }

    /// Programdata handler
    /// \param d
    /// \param ctx
    static void programdataHandler (audiodata *d, void *ctx) {
        (void)ctx;
        std::cerr << "\tstartaddress\t= " << d -> startAddr << "\n";
        std::cerr << "\tlength\t\t= "     << d -> length << "\n";
        std::cerr << "\tsubChId\t\t= "    << d -> subchId << "\n";
        std::cerr << "\tprotection\t= "   << d -> protLevel << "\n";
        std::cerr << "\tbitrate\t\t= "    << d -> bitRate << "\n";
    }
    /// MOT handler
    /// \param s
    /// \param d
    /// \param ctx
    static void	motdataHandler (std::string s, int d, void *ctx) {
        (void)s; (void)d; (void)ctx;
        fprintf (stderr, "MOT %s\n", s. c_str ());
    }

    ///	The function is called from within the library with
    ///	a string, the so-called dynamic label
    /// \param dynamicLabel
    /// \param ctx
    static void dataOut_Handler (std::string dynamicLabel, void *ctx) {
        (void)ctx;
        std::cerr << dynamicLabel << "\r";
    }


    ///	Note: the function is called from the tdcHandler with a
    ///	frame, either frame 0 or frame 1.
    ///	The frames are packed bytes, here an additional header
    ///	is added, a header of 8 bytes:
    ///	the first 4 bytes for a pattern 0xFF 0x00 0xFF 0x00 0xFF
    ///	the length of the contents, i.e. framelength without header
    ///	is stored in bytes 5 (high byte) and byte 6.
    ///	byte 7 contains 0x00, byte 8 contains 0x00 for frametype 0
    ///	and 0xFF for frametype 1
    ///	Note that the callback function is executed in the thread
    ///	that executes the tdcHandler code.
    /// \param data
    /// \param amount
    /// \param type
    /// \param ctx
    static void	bytesOut_Handler (uint8_t *data, int16_t amount,
                              uint8_t type, void *ctx) {
        #ifdef DATA_STREAMER
            uint8_t localBuf [amount + 8];
            int16_t i;
            localBuf [0] = 0xFF;
            localBuf [1] = 0x00;
            localBuf [2] = 0xFF;
            localBuf [3] = 0x00;
            localBuf [4] = (amount >> 8) & 0xFF;
            localBuf [5] = amount & 0xFF;
            localBuf [6] = 0x00;
            localBuf [7] = type == 0 ? 0 : 0xFF;
            for (i = 0; i < amount; i ++)
               localBuf [8 + i] = data;
            tdcServer. sendData (localBuf, amount + 8);
        #else
                (void)data;
                (void)amount;
        #endif
                (void)ctx;
    }


    /// PCM handler
    ///	This function is overloaded. In the normal form it
    ///	handles a buffer full of PCM samples. We pass them on to the
    ///	audiohandler, based on portaudio. Feel free to modify this
    ///	and send the samples elsewhere.
    ///	However, in the "special mode", the aac frames are send out
    ///	Obviously, the parameters "rate" and "isStereo" are meaningless then.
    /// \param buffer
    /// \param size
    /// \param rate
    /// \param isStereo
    /// \param ctx
    static void	pcmHandler (int16_t *buffer, int size, int rate,
                        bool isStereo, void *ctx) {
        static bool isStarted	= false;

        (void)isStereo;
        if (!isStarted) {
            soundOut	-> restart ();
            isStarted	= true;
        }
        soundOut	-> audioOut (buffer, size, rate);
    }

    /// System data handler
    /// \param flag
    /// \param snr
    /// \param freqOff
    /// \param ctx
    static void	systemData (bool flag, int16_t snr, int32_t freqOff, void *ctx) {
//	fprintf (stderr, "synced = %s, snr = %d, offset = %d\n",
//	                    flag? "on":"off", snr, freqOff);
    }
    /// fibQualityhandler
    /// \param q
    /// \param ctx
    static void	fibQuality	(int16_t q, void *ctx) {
//	fprintf (stderr, "fic quality = %d\n", q);
    }
    /// Message quality handler
    /// \param fe
    /// \param rsE
    /// \param aacE
    /// \param ctx
    static void	mscQuality	(int16_t fe, int16_t rsE, int16_t aacE, void *ctx) {
//	fprintf (stderr, "msc quality = %d %d %d\n", fe, rsE, aacE);
    }

    /// Initiates the radio
    /// \param mode Set mode, currently only 1 is supported
    /// \param channel Set initial channel
    /// \param Band Which band to use(BAND_III = VHF range between 174 and 240 mhz)
    /// \param programName Set initial station
    /// \param theGain Set gain level
    /// \param soundchannel Set sound channel
    /// \param latency Set latency
    /// \param waitingtime Set waiting time, this is the amount of time we wait for a signal when scanning
    /// \param autogain If supported set autogain
    int init_radio(uint8_t mode=1, std::string channel="11C", uint8_t Band=BAND_III, std::string programName = "NRK P13",int16_t theGain=80,
                    std::string soundchannel="default",int16_t latency = 10,int16_t waitingtime = 10,bool autogain = false) {
        int16_t ppmcorrection = 0;
        int opt;
        waitingTime = waitingtime;
        timeSynced.store(false);
        timesyncSet.store(false);
        run.store(false);
        sigact.sa_handler = sighandler;
        sigemptyset(&sigact.sa_mask);
        sigact.sa_flags = 0;
        Channel = channel;
        band = Band;
        int32_t frequency = dabBand.Frequency(band, Channel);

        bool err;

        try {
#ifdef    HAVE_SDRPLAY
            theDevice	= new sdrplayHandler (frequency,
                                          ppmcorrection,
                                          theGain,
                                          NULL,
                                          0,
                                          0);
#elif    HAVE_AIRSPY
            theDevice	= new airspyHandler (frequency,
                                         ppmcorrection,
                                         theGain);
#elif    HAVE_RTLSDR
            theDevice	= new rtlsdrHandler (frequency,
                                              ppmcorrection,
                                              theGain,
                                              autogain);
#endif

        }
        catch (int e) {
            std::cerr << "allocating device failed (" << e << "), fatal\n";
            exit(32);
        }
        //
        if (soundOut == NULL) {    // not bound to a file?
            soundOut = new audioSink(latency, soundchannel, &err);
            if (err) {
                std::cerr << "no valid sound channel, fatal\n";
                exit(33);
            }
        }

        theRadio = new dabClass(theDevice,
                                mode,
                                NULL,        // no spectrum shown
                                NULL,        // no constellations
                                syncsignalHandler, //syncsignalHandler
                                systemData, //systemData
                                ensemblenameHandler, //ensemblenameHandler
                                programnameHandler, //programnameHandler
                                fibQuality, //fibQuality
                                pcmHandler, //pcmHandler
                                dataOut_Handler, //dataOut_Handler
                                bytesOut_Handler, //bytesOut_Handler
                                programdataHandler, //programdataHandler
                                mscQuality, //mscQuality
                                motdataHandler,
                                NULL
        );
        if (theRadio == NULL) {
            std::cerr << "sorry, no radio available, fatal\n";
            exit(4);
        }


        theDevice->setGain(theGain);
        if (autogain)
            theDevice->set_autogain(autogain);
        theDevice->setVFOFrequency(frequency);
        theDevice->restartReader();


        //
        //	The device should be working right now

        timesyncSet.store(false);
        ensembleRecognized.store(false);

        return 0;

    }


    /// Scans channels for content
    /// \param mode Specifies which type of scan to perform, quick or full
    /// \param sts  Used to return result to main thread
    /// \param future Used for handling interruptions
    /// \return
    string start_scan(string mode,string* sts,std::future<string> future){
        if(!theRadio){
            init_radio(1,Channel,BAND_III,"radio Norge",80,"default",10,25,true);
            theRadio -> startProcessing();
        }
        vector<string> allChannels = {"5A","5B","5C","5D","6A","6B","6C","6D","7A","7B","7C","7D","8A","8B","8C","8D",
                                   "9A","9B","9C","9D","10A","10B","10C","10D","11A","11B","11C","11D","12A",
                                   "12B","12C","12D","13A","13B","13C","13D","13E","13F"};

        //selected channels, 12D = Nationwide, 12C = Regional(Trøndelag)
        vector<string> selectChannels = {"12C","12D"};
        //channels that contained stations from previous scan
        vector<string> previousChannels = getChannels("channels.txt");
        vector<string> channels;
        if (mode == "quick") channels = selectChannels;
        else if(mode == "full") channels = allChannels;
        else channels = previousChannels;
        station_list = "";
        writeFile("channels.txt","");
        while(future.wait_for(std::chrono::milliseconds(1))== std::future_status::timeout) {
            for (string channel : channels) {
                cout << "Scanning: " + channel << endl;
                found = false;
                int timeOut = 0;
                waitingTime = 3;
                timesyncSet.store(false);
                ensembleRecognized.store(false);
                setChannel(channel);

                //waitingTime is how long we wait for a signal on a channel
                //More time potencially means more stations, but also worse performance
                while (++timeOut < waitingTime)
                    sleep(1);
                if (!timeSynced.load()) {
                    //Low likelyhood of finding stations on channel, continue to next
                    cerr << "There does not seem to be a DAB signal here " << timeSynced << endl;
                    continue;
                } else {
                    //High likelyhood of finding stations
                    //Reset timer so it does not time out before the ensemble with the stations is found
                    std::cerr << "there might be a DAB signal here" << endl;
                }
                if (!ensembleRecognized.load()) {
                    timeOut = 0;
                    waitingTime = 6;
                    if(found) waitingTime += 6;
                    while (!ensembleRecognized.load() && (++timeOut < waitingTime)) {
                        std::cerr << waitingTime - timeOut << "\r";
                        sleep(1);
                    }
                }
                std::cerr << "\n";
                if (!ensembleRecognized.load()) {
                    std::cerr << "no ensemble data found, fatal\n";
                    continue;
                }
            }
            break;
        }
        timesyncSet.store(false);
        ensembleRecognized.store(false);
        //writeToFile("channels.txt",station_list);
        //theRadio -> reset();
        cout << "Finished scan" << endl;
        //get curated stationlist
        string rtr = getStations();
        *sts = rtr;
        return rtr;
    }

    /// Tries to start audio playback on given program
    /// \param programname String variable that contains the name for the wanted program
    /// \param futureobj Future object for controlling purposes
    /// \return
    int play(string programname,std::future<string>futureobj){
        ///If current channel doesn't contain program change channel
        string ch = readFromFile("channels.txt",programname);
        cout << "Current channel: " << Channel << " From file: " << ch << endl;
        if(ch != Channel){
            setChannel(ch);
        }
        /// Run until interrupted
        while(futureobj.wait_for(std::chrono::milliseconds(1))== std::future_status::timeout) {
            run.store(true); /// Controls the playback
            std::cerr << "we try to start program " << programname << "\n";
            if (theRadio->dab_service(programname) < 0) { /// Call to method dab_service in dab-class.cpp
                std::cerr << "sorry  we cannot handle service " << programname << "\n";
                run.store(false);
                return 1;
            }
            while (run.load()) {
                sleep(1);
            }
        }
        run.store(false);
        theRadio->stop();
        return 0;
    }
    /// Stops the device and remove the instances
    void killRadio(){
        theDevice	-> stopReader ();
        theRadio	-> reset ();
        delete theRadio;
        delete theDevice;
        delete soundOut;
    }

    /// Method to extract only stations(not channels) from file
    string getStations(){
        string rtr = "";
        std::istringstream iss(station_list);
        for(string line; getline(iss,line);){
            unsigned long pos = line.find_first_of(",");
            rtr += (line.substr(0,pos)) + "\n";
        }
        return rtr;
    }

    /// Sets a channels frequency as current frequency
    /// \param channel String variable that contains the wanted channel
    void setChannel (string channel){
        int32_t frequency = dabBand.Frequency(BAND_III,channel); /// Find the corresponding frequency
        if(frequency != 0 && theRadio){ /// Continue only if we can find a frequency and the radio is running
            Channel = channel; /// Set current channel to given channel
            theDevice->setVFOFrequency(frequency); /// Set frequency
        }
        cout << "Channel:"<< Channel << "   Frequency:" << theDevice ->getVFOFrequency() << endl;
    }

    /// Uses amixer to make system calls to set Master volume
    /// \param vol String variable that contains either mute, up, down, or a level
    void setVolume(string vol){
        if(vol=="mute"){
            string com_mute = "amixer -q sset Master toggle";
            const char *cmd = com_mute.c_str();
            system(cmd);
        }else if(vol=="up"){
            string com_vol_up = "amixer -q sset Master 7.5%+";
            const char *cmd = com_vol_up.c_str();
            system(cmd);
        }else if (vol=="down"){
            string com_vol_down = "amixer -q sset Master 7.5%-";
            const char *cmd = com_vol_down.c_str();
            system(cmd);
        }else{
            string command = "amixer set Master "+vol+"%";
            const char *cmd = command.c_str();
            system(cmd);
            cout << "Volume set to: " << cmd << endl;
        }
    }

    /// Extracts a specific channel that contained given program
    /// from file that contains the results from previous scan
    /// \param filename String variable that contains the name of the file we want to search
    /// \param programname String variable that contains the name of the program we want to find
    string readFromFile(string filename,string programname){
        ifstream is("channels.txt");
        string str;
        while(getline(is, str)) {
            int pos = str.find_first_of(',');
            if(str.substr(0,pos) == (programname)) {
                is.close();
                cout << "Found channel: " << str.substr(0,pos) << "," << str.substr(pos+1,str.length())<< endl;
                return str.substr(pos+1,str.length());
            }
        }
        is.close();
        return "";
    }

    /// Retrieves all working channels from previous scan from file
    /// \param filename String variable that contains the name of the file we want to search
    vector<string> getChannels(string filename){
        ifstream file;
        file.open(filename);
        vector<string> lst = {""};
        set<string> tmp;
        if (!file) cerr << "Unable to open"+filename;
        char line[255];
        while(file.getline(line,255)){
            string str = line;
            string channel = str.substr(str.find_first_of(",")+1,str.length());
            if(tmp.insert(channel).second) lst.push_back(channel);
        }
        file.close();
        for(auto &i : lst) cout << i << endl;
        return lst;
    }

    /// helper method to write to file
    /// \param filename String variable that contains the name of the file we want to search
    /// \param content String variable that contains the content we want to write
    static void writeToFile(string filename,string content){
        ofstream file;
        std::istringstream iss(content);
        file.open(filename);
        if(!file || !iss) cerr << "Unable to open "+filename;
        for(string line; getline(iss,line);){
            file << line << "\n";
        }
        file.close();
    }
    static void writeFile(string filename, string content, bool append = false) {
        std::ofstream file;
        if(append){
            file.open(filename, std::ios_base::app);
            if (!file) cerr << "Unable to open " << filename << endl;
            file << content;
        }else{
            file.open(filename);
            if (!file) cerr << "Unable to open " << filename << endl;
            file << content;
        }
    }

};


/// Verify that one of the subject alternative names matches the given hostname
bool verify_subject_alternative_name(const char * hostname, X509 * cert) {
    STACK_OF(GENERAL_NAME) * san_names = NULL;

    san_names = (STACK_OF(GENERAL_NAME) *) X509_get_ext_d2i(cert, NID_subject_alt_name, NULL, NULL);
    if (san_names == NULL) {
        return false;
    }

    int san_names_count = sk_GENERAL_NAME_num(san_names);

    bool result = false;

    for (int i = 0; i < san_names_count; i++) {
        const GENERAL_NAME * current_name = sk_GENERAL_NAME_value(san_names, i);

        if (current_name->type != GEN_DNS) {
            continue;
        }

        char * dns_name = (char *) ASN1_STRING_data(current_name->d.dNSName);

        // Make sure there isn't an embedded NUL character in the DNS name
        if (ASN1_STRING_length(current_name->d.dNSName) != strlen(dns_name)) {
            break;
        }
        // Compare expected hostname with the CN
        result = (strcasecmp(hostname, dns_name) == 0);
    }
    sk_GENERAL_NAME_pop_free(san_names, GENERAL_NAME_free);

    return result;
}

/// Verify that the certificate common name matches the given hostname
bool verify_common_name(const char * hostname, X509 * cert) {
    // Find the position of the CN field in the Subject field of the certificate
    int common_name_loc = X509_NAME_get_index_by_NID(X509_get_subject_name(cert), NID_commonName, -1);
    if (common_name_loc < 0) {
        return false;
    }

    // Extract the CN field
    X509_NAME_ENTRY * common_name_entry = X509_NAME_get_entry(X509_get_subject_name(cert), common_name_loc);
    if (common_name_entry == NULL) {
        return false;
    }

    // Convert the CN field to a C string
    ASN1_STRING * common_name_asn1 = X509_NAME_ENTRY_get_data(common_name_entry);
    if (common_name_asn1 == NULL) {
        return false;
    }

    char * common_name_str = (char *) ASN1_STRING_data(common_name_asn1);

    // Make sure there isn't an embedded NUL character in the CN
    if (ASN1_STRING_length(common_name_asn1) != strlen(common_name_str)) {
        return false;
    }

    // Compare expected hostname with the CN
    return (strcasecmp(hostname, common_name_str) == 0);
}

/**
 * This code is derived from examples and documentation found ato00po
 * http://www.boost.org/doc/libs/1_61_0/doc/html/boost_asio/example/cpp03/ssl/client.cpp
 * and
 * https://github.com/iSECPartners/ssl-conservatory
 */
bool verify_certificate(const char * hostname, bool preverified, boost::asio::ssl::verify_context& ctx) {
    // The verify callback can be used to check whether the certificate that is
    // being presented is valid for the peer. For example, RFC 2818 describes
    // the steps involved in doing this for HTTPS. Consult the OpenSSL
    // documentation for more details. Note that the callback is called once
    // for each certificate in the certificate chain, starting from the root
    // certificate authority.

    // Retrieve the depth of the current cert in the chain. 0 indicates the
    // actual server cert, upon which we will perform extra validation
    // (specifically, ensuring that the hostname matches. For other certs we
    // will use the 'preverified' flag from Asio, which incorporates a number of
    // non-implementation specific OpenSSL checking, such as the formatting of
    // certs and the trusted status based on the CA certs we imported earlier.
    int depth = X509_STORE_CTX_get_error_depth(ctx.native_handle());

    // if we are on the final cert and everything else checks out, ensure that
    // the hostname is present on the list of SANs or the common name (CN).
    if (depth == 0 && preverified) {
        X509* cert = X509_STORE_CTX_get_current_cert(ctx.native_handle());

        if (verify_subject_alternative_name(hostname, cert)) {
            return true;
        } else if (verify_common_name(hostname, cert)) {
            return true;
        } else {
            return false;
        }
    }

    return preverified;
}

/// TLS Initialization handler
/**
 * WebSocket++ core and the Asio Transport do not handle TLS context creation
 * and setup. This callback is provided so that the end user can set up their
 * TLS context using whatever settings make sense for their application.
 *
 * As Asio and OpenSSL do not provide great documentation for the very common
 * case of connect and actually perform basic verification of server certs this
 * example includes a basic implementation (using Asio and OpenSSL) of the
 * following reasonable default settings and verification steps:
 *
 * - Disable SSLv2 and SSLv3
 * - Load trusted CA certificates and verify the server cert is trusted.
 * - Verify that the hostname matches either the common name or one of the
 *   subject alternative names on the certificate.
 *
 * This is not meant to be an exhaustive reference implimentation of a perfect
 * TLS client, but rather a reasonable starting point for building a secure
 * TLS encrypted WebSocket client.
 *
 * If any TLS, Asio, or OpenSSL experts feel that these settings are poor
 * defaults or there are critically missing steps please open a GitHub issue
 * or drop a line on the project mailing list.
 *
 * Note the bundled CA cert ca-chain.cert.pem is the CA cert that signed the
 * cert bundled with echo_server_tls. You can use print_client_tls with this
 * CA cert to connect to echo_server_tls as long as you use /etc/hosts or
 * something equivilent to spoof one of the names on that cert
 * (websocketpp.org, for example).
 */
context_ptr on_tls_init(const char * hostname, websocketpp::connection_hdl) {
    context_ptr ctx = websocketpp::lib::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context::tls);

    try {
        ctx->set_options(boost::asio::ssl::context::verify_none|
                         boost::asio::ssl::context::no_sslv2 |
                         boost::asio::ssl::context::no_sslv3 |
                         boost::asio::ssl::context::single_dh_use);


        ctx->set_verify_mode(boost::asio::ssl::verify_none);
        ctx->set_verify_callback(bind(&verify_certificate, hostname, ::_1, ::_2));

        // Here we load the CA certificates of all CA's that this client trusts.
        //ctx->load_verify_file("ca-chain.cert.pem");
    } catch (std::exception& e) {
        std::cout << e.what() << std::endl;
    }
    return ctx;
}

//Method for generating a sha256 hash in string format with openssl
/// https://stackoverflow.com/questions/13784434/how-to-use-openssls-sha256-functions/13784507
string computeSHA256(const string str) {

    unsigned char hash[SHA256_DIGEST_LENGTH];
    SHA256_CTX sha256;
    SHA256_Init(&sha256);
    SHA256_Update(&sha256, str.c_str(), str.size());
    SHA256_Final(hash, &sha256);
    stringstream ss;
    for (int i = 0; i < SHA256_DIGEST_LENGTH; i++) {
        ss << hex << setw(2) << setfill('0') << (int) hash[i];
    }
    return ss.str();
}

/// https://stackoverflow.com/questions/16858782/how-to-obtain-almost-unique-system-identifier-in-a-cross-platform-way
/// Retrieve system machine name
/// \return machine name
static const char* getMachineName(){
    static struct utsname u;
    if ( uname( &u ) < 0 )
    {
        assert(0);
        return "radio_";
    }

    return u.nodename;
}



class connection_listener {

public:
    static inline radio r;
    static inline std::promise<string> exit = std::promise<string>();
    static inline std::future<string> futureobj = exit.get_future();
    static inline string stat = "";
    static inline std::thread* thread = new std::thread(&radio::start_scan,r,"quick",&stat,std::move(futureobj));


    /// On connection open
    /// \param c websocket client
    /// \param hdl websocket connection handler
    static void on_open(client* c, websocketpp::connection_hdl hdl){
        //send a message for authentication
        string payload;
        r.readFromFile("auth.txt",payload);
        if(payload.empty()) {
            string id = getMachineName();
            payload = computeSHA256(id);
            r.writeToFile("auth.txt",payload);
        }
        cout << payload << endl;
        c -> send(hdl, "imaPlayer:"+payload,websocketpp::frame::opcode::text);
        //string res = r.start_scan("quick");
        //c -> send(hdl,"stations:"+res,websocketpp::frame::opcode::text);
    }
    /// On connection close
    /// \param c websocket client
    /// \param hdl websocket connection handler
    static void on_close(client* c, websocketpp::connection_hdl hdl){
        c -> send(hdl, "Goodbye", websocketpp::frame::opcode::text);
        c -> get_alog().write(websocketpp::log::alevel::app, "Connection closed");
        r.killRadio();
        if(thread->joinable()) thread->join();

    }
    /// On connection fail
    /// \param c websocket client
    /// \param hdl websocket connection handler
    static void on_fail(client*c, websocketpp::connection_hdl hdl){
        c -> get_alog().write(websocketpp::log::alevel::app, "Connection Failed");
        r.killRadio();
        if(thread->joinable()) thread->join();
    }
    /// Messagehandler, splits incoming messages into request and data
    /// \param c websocket client
    /// \param hdl websocket connection handler
    /// \param msg message
    static void on_message(client* c, websocketpp::connection_hdl hdl, message_ptr msg) {

        if (msg->get_opcode() == websocketpp::frame::opcode::text) {
            cout << (msg->get_payload()) << endl;
            std::string req = msg -> get_payload();
            //separate payload into request and data value pair
            size_t pos = req.find(":");
            string request = req.substr(0,pos);
            string data = req.substr(pos+1);

            if(request == "authenticated"){
                if(data == "true") cout << "Authenticated" << endl;
                //
            }else if(request == "getStations"){
                string rtr = r.getStations();
                if(rtr.empty() || data == "full"){
                    if(thread->joinable()){
                        //stop running process
                        exit.set_value("");
                    }
                    stat = "";
                    exit = std::promise<string>();
                    futureobj = exit.get_future();
                    thread = new std::thread(&radio::start_scan,r,"quick",&stat,std::move(futureobj));
                    thread->join();
                    cout << "Thread finished," << " stations: " << stat << endl;
                    if(stat.empty()){
                        cout << "No stations found, performing full scan" << endl;
                        exit = std::promise<string>();
                        futureobj = exit.get_future();
                        thread = new std::thread(&radio::start_scan,r,"full",&stat,std::move(futureobj));
                        thread->join();
                    }
                    rtr = stat;
                }
                c -> send(hdl,"stations:"+rtr,websocketpp::frame::opcode::text);
            }
            else if(request == "playStation"){
                //Stop existing thread
                if(thread->joinable()){
                    //stop running process
                    exit.set_value("");
                }
                exit = std::promise<string>();
                futureobj = exit.get_future();
                thread = new std::thread(&radio::play,r,data,std::move(futureobj));
            }else if(request == "scan"){
                if(thread->joinable()){
                    //stop running process
                    exit.set_value("");
                }
                stat = "";
                exit = std::promise<string>();
                futureobj = exit.get_future();
                thread = new std::thread(&radio::start_scan,r,"full",&stat,std::move(futureobj));
                thread->join();
                cout << "Thread finished," << " stations: " << stat << endl;
                c -> send(hdl,"stations:"+stat,websocketpp::frame::opcode::text);
            }else if(request == "volume") {
                cout << "volume: " << data << endl;
                r.setVolume(data);
            }else if(request == "room"){
                if(data =="false") r.setVolume("mute");
                //c -> send(hdl,""+data,websocketpp::frame::opcode::text);
            }else if(request == "mute"){
                r.setVolume("mute");
            }else{
                cout << "request: " << request << " not recognized" << endl;
                c -> send(hdl,"request: "+ request + " is not supported",websocketpp::frame::opcode::text);
            }
        } else {
            cout << (websocketpp::utility::to_hex(msg->get_payload())) << endl;
        }
    }       
};

int	main (int argc, char **argv) {

    std::string hostname = "18.188.200.130";
    std::string port = "3000";
    std::string uri = "https://" + hostname + ":" + port;

    client c;

    try {

        // Set logging to be pretty verbose (everything except message payloads)
        c.set_access_channels(websocketpp::log::alevel::all);
       // c.clear_access_channels(websocketpp::log::alevel::frame_payload);
        c.set_error_channels(websocketpp::log::elevel::all);

        // Initialize ASIO
        c.init_asio();
        c.start_perpetual();

        // Register handlers
        c.set_message_handler(bind(connection_listener::on_message, &c, ::_1, ::_2));
        c.set_close_handler(bind(connection_listener::on_close,&c,::_1));
        c.set_tls_init_handler(bind(&on_tls_init, hostname.c_str(), ::_1));
        c.set_fail_handler(bind(connection_listener::on_fail,&c,::_1));
        c.set_open_handler(bind(connection_listener::on_open,&c,::_1));

        websocketpp::lib::error_code ec;
        client::connection_ptr con = c.get_connection(uri,ec);
        if (ec) {
            std::cout << "could not create connection because: " << ec.message() << std::endl;
            return 0;
        }

        // Note that connect here only requests a connection. No network messages are
        // exchanged until the event loop starts running in the next line.
        c.connect(con);

        c.get_alog().write(websocketpp::log::alevel::app, "Connecting to " + uri);

        // Start the ASIO io_service run loop
        // this will cause a single connection to be made to the server. c.run()
        // will exit when this connection is closed.
        c.run();

    } catch (websocketpp::exception const & e) {
        std::cout << e.what() << std::endl;
        connection_listener::r.killRadio();
        c.stop();
    }

    c.stop();

    return 0;
}






